
import Routing from 'fos-router';

$(document).ready(function() {
    $(document).on("click","#btnFiltreCategorie",function() {
        var cat = $("#catSelect").val();
        if (cat > 0){
            var url = Routing.generate('app_produit_by_category', { cat: cat }, false);
            $.ajax({
                type: 'get',
                url: url,
                dataType : 'html',
                success: function (result) {
                    $("#tableListeProduits").empty();
                    $("#tableListeProduits").html(result);
                }
            });
        } else {
            var urlProduit = Routing.generate('app_produit_index');
            location = urlProduit;
        }
    });
});