<?php

namespace App\Service;

use App\Entity\Categorie;
use App\Entity\Produit;
use App\Repository\CategorieRepository;
use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\Collection;

class QueryService 
{
    public function __construct(
        private CategorieRepository $categorieRepository,
        private ProduitRepository   $produitRepository
    ) {}

    public function findCategorie(int $categorieId): ?Categorie 
    {
        return $this->categorieRepository->find($categorieId);
    }

    public function getAllCategory(): array 
    {
        return $this->categorieRepository->findAll();
    }

    public function findProductsByCategory(string $nom): Collection 
    {
        return $this
            ->categorieRepository
            ->findOneBy(['nom' => $nom])
            ->getProduits();
    }

    public function findAllProducts(): array 
    {
        return $this->produitRepository->findAll();
    }

    public function findProductsByNom(string $nom): array 
    {
        return $this->produitRepository->findBy(['nom' => $nom]);
    }

    public function findProductById(int $produitId): ?Produit
    {
        return $this->produitRepository->find($produitId);
    }
}