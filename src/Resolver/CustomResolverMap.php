<?php

namespace App\Resolver;

use App\Service\MutationService;
use App\Service\QueryService;
use ArrayObject;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\ArgumentInterface;
use Overblog\GraphQLBundle\Resolver\ResolverMap;

class CustomResolverMap extends ResolverMap 
{
    public function __construct(
        private QueryService    $queryService,
        private MutationService $mutationService
    ) {}

    /**
     * @inheritDoc
     */
    protected function map(): array 
    {
        return [
            'RootQuery'    => [
                self::RESOLVE_FIELD => function (
                    $value,
                    ArgumentInterface $args,
                    ArrayObject $context,
                    ResolveInfo $info
                ) {
                    return match ($info->fieldName) {
                        'categorie' => $this->queryService->findCategorie((int)$args['id']),
                        'categories' => $this->queryService->getAllCategory(),
                        'findProductsByCategory' => $this->queryService->findProductsByCategory($args['nom']),
                        'produits' => $this->queryService->findAllProducts(),
                        'findProductsByNom' => $this->queryService->findProductsByNom($args['nom']),
                        'produit' => $this->queryService->findProductById((int)$args['produitId']),
                        default => null
                    };
                },
            ],
            'RootMutation' => [
                self::RESOLVE_FIELD => function (
                    $value,
                    ArgumentInterface $args,
                    ArrayObject $context,
                    ResolveInfo $info
                ) {
                    return match ($info->fieldName) {
                        'createCategorie' => $this->mutationService->createCategorie($args['categorie']),
                        'updateProduit' => $this->mutationService->updateProduit((int)$args['id'], $args['produit']),
                        default => null
                    };
                },
            ],
        ];
    }
}