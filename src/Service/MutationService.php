<?php

namespace App\Service;

use App\Entity\Categorie;
use App\Entity\Produit;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Error\Error;

class MutationService 
{
    public function __construct(
        private EntityManagerInterface $manager
    ) {}

    public function createCategorie(array $categorieDetails): Categorie {

        $categorie = new Categorie(
            $categorieDetails['nom']
        );

        $this->manager->persist($categorie);
        $this->manager->flush();

        return $categorie;
    }

    public function updateProduit(int $produitId, array $newDetails): Produit 
    {
        $produit = $this->manager->getRepository(Produit::class)->find($produitId);

        if (is_null($produit)) {
            throw new Error("Produit introuvable pour cet ID");
        }
    
        foreach ($newDetails as $property => $value) {
            if ($property === 'nom') {
                $produit->setNom($value);
            } else if ($property === 'prix') {
                $produit->setPrix($value);
            } else if ($property === 'quantite') {
               $produit->setQuantite($value); 
            }
        }

        $this->manager->persist($produit);
        $this->manager->flush();

        return $produit;
    }
}