<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;
use App\Entity\Produit;
use App\Entity\Categorie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ProduitFixtures extends Fixture
{

    private Generator $faker;

    public function __construct() 
    {
        $this->faker = Factory::create('fr_FR');
    }
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 10; $i++) {
            $cat = $this->getFakeCategorie();
            $manager->persist($cat);
            for ($j = 0; $j < random_int(2,5); $j++) {
                $produit = $this->getFakeProduit();
                $produit->setCategorie($cat);
                $manager->persist($produit);
            }
        }

        $manager->flush();
    }

    private function getFakeCategorie(){
        $categorie = new Categorie();
        $categorie->setNom($this->faker->text(10));
        return $categorie;
    }

    private function getFakeProduit(){
        $produit = new Produit();
        $produit->setNom($this->faker->text(15));
        $produit->setPrix($this->faker->numberBetween(100, 10000));
        $produit->setQuantite($this->faker->numberBetween(1, 100));
        $produit->setCategorie($this->getFakeCategorie(25));
        return $produit;
    }
}